--Membuat Database--
CREATE DATABASE kampus;


--Membuat Tabel kampus--
CREATE TABLE mahasiswa (
    nim INT PRIMARY KEY,            
    nama VARCHAR(100),
    kode_matakuliah INT NOT NULL
);

--Membuat tabel Mata_kuliah

CREATE TABLE mata_kuliah(
    kode_matakuliah BIGSERIAL PRIMARY KEY NOT NULL,
    mata_kuliah VARCHAR(100) NOT NULL,
    kode_kelas INT NOT NULL
);

--MEMBUAT TABEL DOSEN
CREATE TABLE dosen(
    id_dosen INT PRIMARY KEY,
    nama VARCHAR(100) NOT NULL,
    kode_matakuliah INT NOT NULL
);

--MEMBUAT TABEL KELAS 
CREATE TABLE kelas(
    kode_kelas INT PRIMARY KEY,
    nama_kelas VARCHAR(100) NOT NULL,
    gedung VARCHAR(100) NOT NULL
);

-- INSERT DATA KEDALAM TABEL--
-- MAHASISWA --
INSERT INTO mahasiswa (
    nim, nama, kode_matakuliah
)
VALUES 
(200204206, 'Andika', 513412),
(200204207, 'Tejo', 24123), 
(200204208, 'Suep', 85678),
(200204209, 'Japari', 890123),
(200204210, 'Sudarmin', 90123);

-- TABEL MATA_KULIAH --
INSERT INTO mata_kuliah (
    kode_matakuliah, mata_kuliah, kode_kelas
)
VALUES
    (907856,'Kalkulus',560156),
    (670134,'Sistem informasi',896790),
    (901245,'Matemtika diskret',901876),
    (689012,'Stastistika',890786),
    (781923, 'Melupakan kenanganmu',890167);

--  TABEL DOSEN -- 
INSERT INTO dosen (
    id_dosen, nama, kode_matakuliah
)
VALUES
(200213921, 'Dedi', 891254),
(200216789, 'Fatah', 891456), 
(200289516, 'Devi', 670987),
(200278921, 'Helmi', 67019),
(2002678134, 'Dimas', 891267);

-- TABEL KELAS --
INSERT INTO kelas (
    kode_kelas, nama_kelas, gedung
)
VALUES
(1456,'lab_rpl','siwal'),
(1978,'lab_si','fki'),
(8917,'lab_jaringan','fki'),
(8910,'lab_kalkulus','siwal');

-- Update Data Mahasiswa
UPDATE mahasiswa
SET nama = "Andika Kangen Band"
WHERE nim = 200204206;

-- Delete Data Dosen

DELETE FROM dosen 
WHERE name = 'Helmi'; 

SELECT *FROM mahasiswa



                                                                                                                                                                                                 
                                                                                                                                                                                                        