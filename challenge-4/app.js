const express = require('express')
const app = express();
const router = require('./routes');
const morgan = require('morgan');
const HTTP_PORT = 3000;
app.use(express.json())
app.use(morgan('dev'))
app.use(router)

app.listen(HTTP_PORT, () => {
    console.log("listening the port", HTTP_PORT);
});