const auth = require("./auth");
const userBio = require("./userBio");
const userHistory = require("./userHistory");
module.exports = {
    auth,
    userBio,
    userHistory,
};