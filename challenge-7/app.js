require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const router = require("./routes");
const cookieParser = require("cookie-parser");
const app = express();
const { HTTP_PORT } = process.env;

app.use(express.json());

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.set("view engine", "ejs");

app.use("/api", router);

// handling 404
app.use((req, res, next) => {
    return res.status(404).json({
        status: false,
        message: "Are You Lost?",
    });
});

// handling 500
app.use((req, res, next) => {
    return res.status(500).json({
        status: false,
        message: err.message,
    });
});

app.listen(HTTP_PORT, () => {
    console.log("Listening On Port", HTTP_PORT);
});