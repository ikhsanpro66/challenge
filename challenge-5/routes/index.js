const express = require("express");
const router = express.Router();
const auth = require("./auth");
const userbio = require("./userbio");
const userHistory = require("./userHistory");

router.use("/auth", auth);
router.use("/userBio", userbio);
router.use("/userHistory", userHistory);

module.exports = router;