"use strict";
const bcrypt = require("bcrypt");

module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.bulkInsert(
            "Users", [{
                name: "John Doe",
                email: "jhonDoe@gmail.com",
                password: await bcrypt.hash("admin123", 10),
                user_type: "BASIC",
                role: "ADMIN",
                createdAt: new Date(),
                updatedAt: new Date(),
            }, ], {}
        );
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.bulkDelete("Users", null, {});
    },
};